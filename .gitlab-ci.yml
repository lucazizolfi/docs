# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: Huawei Inc.

stages:
  - compliance
  - test
  - build
  - deploy

include:
 - project: eclipse/oniro-core/oniro
   ref: kirkstone
   file:
    - .oniro-ci/dco.yaml
    - .oniro-ci/reuse.yaml

dco:
  extends: .dco
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'

reuse:
  extends: .reuse
  allow_failure: true
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'


# Naming scheme for variables is defined as follows:
#
# CI_ONIRO_*:
#   Oniro specific variables used during CI/CD process. Variables in this group
#   should use meaningful names and avoid abbreviations, if possible.
#
# CI_ONIRO_MANIFEST_REPO_{URL,REV}:
#   URL and revision of the manifest repository.
#
# CI_ONIRO_MANIFEST_MIRROR_REPO_{URL,REV,DIR}:
#   URL, revision and directory path of the mirror of manifest repository.
#   This repository is used to speed up construction of repo workspace.
#
# CI_ONIRO_REPO_WORKSPACE_DIR:
#   Directory path of repo workspace.
#
# CI_*:
#   Third party variables used during CI/CD process, defined by GitLab.
#   Variables in this group are defined by GitLab and retain their original
#   name.
#
# GIT_STRATEGY, CACHE_COMPRESSION_LEVEL:
#   Part of GitLab interface.
.oniro-repo-workspace:
  interruptible: true
  image:
    name: registry.ostc-eu.org/ostc/oniro/bitbake-builder:latest
  variables:
    # URL and branch or revision of the oniro.git repository which contains a
    # repo manifest file.
    #
    # The revision should be bumped during the major release of Oniro but both
    # variables can be changed to CI_PROJECT_URL/CI_COMMIT_SHA when testing
    # changes landing to oniro.git.
    CI_ONIRO_MANIFEST_REPO_URL: https://gitlab.eclipse.org/eclipse/oniro-core/oniro.git
    CI_ONIRO_MANIFEST_REPO_REV: kirkstone
    # URL and branch used with repo "repo --mirror" to speed up workspace
    # construction.
    #
    # Those are distinct from CI_ONIRO_MANIFEST_REPO_{URL,REV} because the
    # former variables can be redirected to CI_PROJECT_URL and CI_COMMIT_SHA,
    # while those two stay fixed.
    #
    # The revision should _only_ be bumped during the major release of Oniro.
    CI_ONIRO_MANIFEST_MIRROR_REPO_URL: https://gitlab.eclipse.org/eclipse/oniro-core/oniro.git
    CI_ONIRO_MANIFEST_MIRROR_REPO_REV: kirkstone
    # Directory where repo mirror is constructed. This location is covered by
    # GitLab cache system, and will be reused between pipelines of the same
    # project. Note that the .cache directory name is special.
    CI_ONIRO_MANIFEST_MIRROR_REPO_DIR: $CI_PROJECT_DIR/.cache/repo-mirror
    # XML snippet to inject as a "local manifest" for repo. Those allow arbitrary
    # modifications to the project structure to happen before "repo sync" is used
    # to construct the workspace.
    #
    # The default interpreter for the local manifest is plain "echo". For some
    # more complex cases, where inline shell is required, use "eval" instead
    # and put "cat" echo into the local manifest, coupled with a here-doc
    # value.
    CI_ONIRO_REPO_WORKSPACE_LOCAL_MANIFEST: ""
    CI_ONIRO_REPO_WORKSPACE_LOCAL_MANIFEST_INTERPRETER: echo
    # Directory where repo workspace is constructed.
    CI_ONIRO_REPO_WORKSPACE_DIR: $CI_PROJECT_DIR/.tmp/repo-workspace
    # Use fastest cache compression algorithm, as bulk of the cache is
    # already-compressed git history.
    CACHE_COMPRESSION_LEVEL: fastest
    # Ask GitLab _not_ to check out the git repository associated with the
    # project. This is, in a way, pointless, since we use repo, not pure git,
    # to construct the workspace. Due to the fact that oniro is
    # self-referential (the manifest refers to the repository that contains the
    # manifest). This requires custom logic to behave correctly in scenarios
    # that modify oniro.git in any way (e.g. a branch, a pull request or merge
    # train).
    GIT_STRATEGY: none
  cache:
    - key:
        prefix: repo-mirror-$CI_ONIRO_MANIFEST_MIRROR_REPO_REV
        files:
          - default.xml
      paths:
        - $CI_ONIRO_MANIFEST_MIRROR_REPO_DIR
  before_script:
    # Define helper functions to generate GitLab fold markers.
    - |

      function gl_section_open() {
        printf '\e[0K''section_start'':%s:%s\r\e[0K%s\n' "$(date +%s)" "$1" "$2"
      }

      function gl_section_open_collapsed() {
        printf '\e[0K''section_start'':%s:%s[collapsed=true]\r\e[0K%s\n' "$(date +%s)" "$1" "$2"
      }

      function gl_section_close() {
        printf '\e[0K''section_end'':%s:%s\r\e[0K\n' "$(date +%s)" "$1"
      }

    # Query system information. This is mostly useful for forensics, when
    # something goes wrong and access to basic information of this type can
    # help to uncover the problem.
    - gl_section_open_collapsed system_info "Querying system information"
    - id
    - uname -a
    - cat /etc/os-release
    - free -m
    - lscpu
    - env | grep -E '^CI_ONIRO' | sort
    - gl_section_close system_info

    # Set up Git with bot identity. Eclipse ECA check allows this user to
    # create and send commits.
    - gl_section_open_collapsed setup_git "Setting up git"
    - git config --global --add safe.directory "$CI_PROJECT_DIR"
    - git config --global user.name "Oniro Core Project Bot"
    - git config --global user.email "oniro-core-bot@eclipse.org"
    - gl_section_close setup_git

    # Since CI_PROJECT_DIR is set to 'none', GitLab runner does not perform any
    # cleanup operations on CI_PROJECT_DIR. In consequence, repo can observe
    # junk brought in by previous executions on the same runner, and get
    # confused.  Perform manual cleanup by removing all top-level items, other
    # than .cache, where the cache items are strategically located, before
    # proceeding.
    - gl_section_open_collapsed cleanup_project_dir "Clean-up project directory"
    - find "$CI_PROJECT_DIR" -mindepth 1 -maxdepth 1 ! -name .cache -exec rm -rf {} \;
    - ls -la "$CI_PROJECT_DIR"
    - gl_section_close cleanup_project_dir

    # Create and update a mirror for repo, using the semi-fixed manifest mirror
    # repo URL and revision. Since this is cached, the "repo init" part is
    # rarely executed (see the test command below), and only the forced
    # synchronization is executed.
    #
    # Note that the location of the mirror is stored in GitLab cache using the
    # repo revision as cache key, allowing multiple releases to co-exist
    # efficiently.
    - gl_section_open_collapsed repo_mirror_setup "Setting up repo mirror"
    - mkdir -p "$CI_ONIRO_MANIFEST_MIRROR_REPO_DIR"
    - pushd "$CI_ONIRO_MANIFEST_MIRROR_REPO_DIR"
    - echo "Initializing repository mirror from $CI_ONIRO_MANIFEST_MIRROR_REPO_URL and $CI_ONIRO_MANIFEST_MIRROR_REPO_REV"
    - test ! -e .repo && repo init --mirror --manifest-url "$CI_ONIRO_MANIFEST_MIRROR_REPO_URL" --manifest-branch "$CI_ONIRO_MANIFEST_MIRROR_REPO_REV" --no-clone-bundle
    - echo "Synchronizing repository mirror"
    - repo sync --force-sync || ( rm -rf .repo && repo init --mirror --manifest-url "$CI_ONIRO_MANIFEST_MIRROR_REPO_URL" --manifest-branch "$CI_ONIRO_MANIFEST_MIRROR_REPO_REV" --no-clone-bundle && repo sync)
    - gl_section_close repo_mirror_setup

    # Create a repo workspace using the mirror as reference. This is fairly
    # efficient, as repo will hardlink files (assuming they live on the same
    # filesystem) and avoid bulk of the network traffic.
    - gl_section_open_collapsed repo_workspace_setup "Setting up repo workspace"
    - rm -rf "$CI_ONIRO_REPO_WORKSPACE_DIR" && mkdir -p "$CI_ONIRO_REPO_WORKSPACE_DIR"
    - pushd "$CI_ONIRO_REPO_WORKSPACE_DIR"
    - echo "Initializing repository workspace from $CI_ONIRO_MANIFEST_REPO_URL and $CI_ONIRO_MANIFEST_REPO_REV"
    - repo init --reference "$CI_ONIRO_MANIFEST_MIRROR_REPO_DIR" --manifest-url "$CI_ONIRO_MANIFEST_REPO_URL" --manifest-branch "$CI_ONIRO_MANIFEST_REPO_REV" --no-clone-bundle
    - mkdir -p "${CI_ONIRO_REPO_WORKSPACE_DIR}/.repo/local_manifests"
    - test -n "${CI_ONIRO_REPO_WORKSPACE_LOCAL_MANIFEST:-}" && "$CI_ONIRO_REPO_WORKSPACE_LOCAL_MANIFEST_INTERPRETER" "$CI_ONIRO_REPO_WORKSPACE_LOCAL_MANIFEST" | tee "${CI_ONIRO_REPO_WORKSPACE_DIR}/.repo/local_manifests/local.xml"
    - echo "Synchronizing repository workspace"
    - repo sync --force-sync
    - gl_section_close repo_workspace_setup

build-docs:
  extends: [.oniro-repo-workspace]
  interruptible: true
  image:
    name: registry.ostc-eu.org/ostc/oniro/docs-builder:latest
  script:
    - make -C docs
    - mv docs/build "$CI_PROJECT_DIR"
  artifacts:
    paths:
      - build
  variables:
    # When the workspace is created, substitute the "docs" repository that
    # described by the manifest with the project being tested. This works for
    # forks and branches but not for merge requests. For that look at the build
    # rule below.
    CI_ONIRO_REPO_WORKSPACE_LOCAL_MANIFEST: >
      <?xml version="1.0" encoding="UTF-8"?>
      <manifest>
        <!-- remove original docs project entry -->
        <remove-project name="oniro-core/docs.git" />
        <!-- add remote representing the project -->
        <remote name="oniro-override" fetch="${CI_PROJECT_URL}/../" />
        <!-- add docs at the exact version are testing -->
        <project name="${CI_PROJECT_NAME}" path="docs" remote="oniro-override" revision="${CI_COMMIT_SHA}" />
      </manifest>
  rules:
    # During the merge request, substitute the "docs" repository that is
    # described by the manifest with the project that is the source of the
    # merge request. This does not test the merged result but is the next best
    # thing we can do right now.
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      variables:
        CI_ONIRO_REPO_WORKSPACE_LOCAL_MANIFEST_INTERPRETER: eval
        CI_ONIRO_REPO_WORKSPACE_LOCAL_MANIFEST: |
          cat <<__EOM__
          <?xml version="1.0" encoding="UTF-8"?>
          <manifest>
            <!-- remove original docs project entry -->
            <remove-project name="oniro-core/docs.git" />
            <!-- add remote representing the project -->
            <remote name="oniro-override" fetch="${CI_MERGE_REQUEST_SOURCE_PROJECT_URL}/../" />
            <!-- add docs at the exact version are testing -->
            <project name="$(basename "$CI_MERGE_REQUEST_SOURCE_PROJECT_PATH")" path="docs" remote="oniro-override" revision="${CI_COMMIT_SHA}" />
          </manifest>
          __EOM__
    # Or when things land.
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'

deploy:
  extends: .oniro-repo-workspace
  image:
    name: registry.ostc-eu.org/ostc/oniro/docs-builder:latest
  stage: deploy
  script:
    # We are in the root of the git-repo workspace. Because
    # .oniro-repo-workspace uses GIT_STRATEGY=none, the workspace is not
    # cleaned automatically.
    - rm -rf aggregated
    - git clone https://user:$CI_ONIRO_AGGREGATED_DOCS_TOKEN@gitlab.eclipse.org/eclipse/oniro-core/oniro-readthedocs-aggregated.git aggregated
    - find aggregated -maxdepth 1 -not -path aggregated/.git -not -path aggregated -exec rm -rvf {} \;
    - tar -c --dereference -C docs --exclude ./.git --exclude ./.gitlab-ci.yml . | tar -x -C aggregated
    # Commit and push back, if something changed.
    - |
      cd aggregated;
      if [ -n "$(git status -s)" ]; then
        msg="docs repository snapshot - $CI_COMMIT_BRANCH:$CI_COMMIT_SHA";
        git add -A;
        git commit -sm "$msg";
        git push origin HEAD:main;
      else
        echo "Nothing new to commit.";
      fi
  rules:
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'

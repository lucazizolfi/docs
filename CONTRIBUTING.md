<!--
SPDX-FileCopyrightText: Huawei Inc.

SPDX-License-Identifier: CC-BY-4.0
-->

- <a href="#eclipse-contributor-agreement" id="toc-eclipse-contributor-agreement">Eclipse Contributor Agreement</a>
- <a href="#gitlab-contributions" id="toc-gitlab-contributions">Gitlab Contributions</a>
  - <a href="#overview" id="toc-overview">Overview</a>
  - <a href="#git-setup" id="toc-git-setup">Git setup</a>
  - <a href="#commit-guidelines" id="toc-commit-guidelines">Commit Guidelines</a>
  - <a href="#contributions-to-documentation" id="toc-contributions-to-documentation">Contributions to Documentation</a>
    - <a href="#creating-merge-requests" id="toc-creating-merge-requests">Creating merge requests</a>
- <a href="#reuse-compliance" id="toc-reuse-compliance">REUSE Compliance</a>
  - <a href="#spdx-information-and-reuse-standard" id="toc-spdx-information-and-reuse-standard">SPDX Information and REUSE Standard</a>
    - <a href="#spdx-header-example" id="toc-spdx-header-example">SPDX Header Example</a>
    - <a href="#dep5-files-paragraph-examples" id="toc-dep5-files-paragraph-examples">DEP5 "Files" Paragraph Examples</a>
    - <a href="#substantial-contributions" id="toc-substantial-contributions">Substantial Contributions</a>
- <a href="#dco-sign-off" id="toc-dco-sign-off">DCO sign-off</a>
  - <a href="#overview-1" id="toc-overview-1">Overview</a>
  - <a href="#docs_dco" id="toc-docs_dco">Developer Certificate of Origin</a>

# Eclipse Contributor Agreement

Before your contribution can be accepted by the project team, contributors must electronically sign the [Eclipse Contributor Agreement (ECA)](http://www.eclipse.org/legal/ECA.php).

Commits must have a Signed-off-by field in the footer indicating that the author is aware of the terms by which the contribution has been provided to the project. Also, an associated Eclipse Foundation account needs to be in place with a signed Eclipse Contributor Agreement on file. These requirements are enforced by the Eclipse Foundation infrastructure tooling.

For more information, please see the [Eclipse Committer Handbook](https://www.eclipse.org/projects/handbook/#resources-commit).

# Gitlab Contributions

## Overview

Oniro Project handles contributions as [merge requests](https://docs.gitlab.com/ee/user/project/merge_requests/) to relevant repositories part of the Oniro Project [GitLab instance](https://gitlab.eclipse.org/eclipse/oniro-core). The flow for handling that is classic: fork-based merge requests. This means that once you have an account, you can fork any repository, create a branch with proposed changes and raise a merge request against the forked repository. More generic information you can find on the Gitlab's documentation as part of ["Merge requests workflow"](https://docs.gitlab.com/ee/development/contributing/merge_request_workflow.html).

## Git setup

Clone your fork locally, enter its directory and set:

``` bash
$ git config --local user.email <your_eclipse_account_email>
$ git config --local user.name <your_eclipse_full_name>
```

To push and pull over HTTPS with Git using your account, you must set a password or [a Personal Access Token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) .

If you want to push or pull repositories using SSH, you have to [add a SSH key](https://docs.gitlab.com/ee/user/ssh.html) to your profile.

## Commit Guidelines

<div class="note">

<div class="title">

Note

</div>

If you are new to `git`, start by reading the official [Getting Started Document](https://git-scm.com/book/en/v2/Getting-Started-First-Time-Git-Setup).

</div>

At its core, contributing to the Oniro Project project means *wrapping* your work as `git` commits. How we handle this has an impact on rebasing, cherry-picking, back-porting, and ultimately exposing consistent documentation through its logs.

To achieve this, we maintain the following commit guidelines:

- Each commit should be able to stand by itself providing a building block as part of the MR.
  - A good balance of granularity with scoped commits helps to handle backports (e.g. cherry-picks) and also improves the ability to review smaller chunks of code taking commit by commit.
- Changes that were added on top of changes introduced in the MR, should be squashed into the initial commit.
  - For example, a MR that introduced a new build system recipe and, as a separate commit, fixed a build error in the initial recipe. The latter commit should be squashed into the initial commit.
  - For example, a MR introducing a new docs chapter and also adding, as a separate commit, some typo fixes. The latter commits should be squashed into the initial commit.
  - There is a small set of exceptions to this rule. All these exceptions gravitate around the case where an MR, even if it provides multiple commits in the same scope (for example, to the same build recipe), each of the commits has a very specific purpose.
    - For example, a line formating change followed by a chapter addition change in the same documentation file.
    - Also, it can be the case of two functional changes that are building blocks in the same scope.
    - Another example where commits are not to be squashed is when having a commit moving the code and a commit modifying the code in the new location.
- Make sure you clean your code of trailing white spaces/tabs and that each file ends with a new line.
- Avoid *merge* commits as part of your MR. Your commits should be rebased on top of the *HEAD* of the destination branch.

As mentioned above, *git log* becomes informally part of the documentation of the product. Maintaining consistency in its format and content improves debugging, auditing, and general code browsing. To achieve this, we also require the following commit message guidelines:

- The *subject* line (the first line) needs to have the following format: `scope: Title limited to 80 characters`.
  - Use the imperative mood in the *subject* line for the *title*.
  - The *scope* prefix (including the colon and the following whitespace) is optional but most of the time highly recommended. For example, fixing an issue for a specific build recipe, would use the recipe name as the *scope*.
  - The *title* (the part after the *scope*) starts with a capital letter.
  - The entire *subject* line shouldn't exceed 80 characters (same text wrapping rule for the commit body).
- The commit *body* separated by an empty line from the *subject* line.
  - The commit *body* is optional but highly recommended. Provide a clear, descriptive text block that accounts for all the changes introduced by a specific commit.
  - The commit *body* must not contain more than 80 characters per line.
- The commit message will have the commit message *trailers* separated by a new line from the *body*.
  - Each commit requires at least a *Signed-off-by* trailer line. See more as part of the `/contributing/dco` document.
  - All *trailer* lines are to be provided as part of the same text block - no empty lines in between the *trailers*.

Additional commit message notes:

- Avoid using special characters anywhere in the commit message.
- Be succinct but descriptive.
- Have at least one *trailer* as part of each commit: *Signed-off-by*.
- You can automatically let `git` add the *Signed-off-by* by taking advantage of its `-s` argument.
- Whenever in doubt, check the existing log on the file (`<FILE>`) you are about to commit changes, using something similar to: `git log <FILE>`.

Example of a full git message:

``` text
busybox: Add missing dependency on virtual/crypt

Since version 1.29.2, the busybox package requires virtual/crypt. Add this
to DEPENDS to make sure the build dependency is satisfied.

Signed-off-by: Joe Developer <joe.developer@example.com>
```

## Contributions to Documentation

In Oniro Project, the documentation usually stays with the respective code repositories. This means that contributing to documentation is not in any way different than contributing to code. The processes, contribution guidelines are to remain the same. The only difference is that documentation files are to be released under `Creative Commons License version 4.0`.

Documentation that doesn't link directly to one specific repository, is available in the [docs repository](https://gitlab.eclipse.org/eclipse/oniro-core/docs).

In terms of file format, the project unifies its documentation as `ReStructuredText` files. A RestructuredText primer is available as part of the Sphinx [documentation](https://www.sphinx-doc.org/en/master/usage/restructuredtext/basics.html).

As a rule of thumb, anything that ends up compiled in the project documentation is to maintain the RestructuredText file format. Text files that are not meant to be compiled as part of the project's documentation can be written in [Markdown](https://daringfireball.net/projects/markdown/). For example, a repository `README` file can be written in Markdown as it doesn't end up compiled in the project-wide documentation.

### Creating merge requests

Once your changes have been pushed to your fork, you are ready to prepare a merge request.

1.  Go to your repository in an internet browser.
2.  Create a merge request by clicking `Merge Requests` on left toolbar and press `New merge request`. Add an explainable description and create a merge request. Alternatively, you can enter the website of your fork. You should see a message that you pushed your branch to the repository. In the same section you can press `Create merge request`.
3.  Before merging, it has to be reviewed and approved by Oniro Project repository maintainers. Read their review and add any required changes to your merge request.
4.  After you polish your merge request, the maintainers will run the pipelines which check if your changes do not break the project and approve them. If everything is correct, your work is merged to the main project. Remember that each commit of the merge request should be a minimum, self-contained building block.

# REUSE Compliance

## SPDX Information and REUSE Standard

All projects and files for an hosted project **MUST** be [REUSE](https://reuse.software/) compliant. REUSE requires SPDX information for each file, rules for which are as follows:

- for files copyrighted by projects contributors (**"First Party Files"**):
  - any new file MUST have a SPDX header (copyright and license);
  - for files that don't support headers (for example binaries, patches etc.) an associated `.license` file MUST be included with the relevant SPDX information;
  - do not add Copyright Year as part of the SPDX header information;
  - the general rule for patch files is to use the MIT license and *not* the license of the component for which the patch applies - the latter solution would be error-prone and hard to manage and maintain in the long run, and there may be difficult-to-handle cases (what if the patches modifies multiple files in the same component - eg. gcc - which are subject to different licenses?);
  - when modifying a file through this contribution process, you may (but don't have to) claim copyright by adding a copyright line;
  - you MUST NOT alter copyright statements made by others, but only add your own;
- for files copyrighted by third parties and just added to the project by contributors, eg. files copied from other projects or back-ported patches (**"Third Party Files"**):
  - if upstream files already have SPDX headers, they MUST be left unchanged;
  - if upstream files do *not* have SPDX headers:
    - the exact upstream provenance (repo, revision, path) MUST be identified;
    - you MUST NOT add SPDX headers to Third Party Files;
    - copyright and license information, as well as upstream provenance information (in the "Comment" section), MUST be stored in <span class="title-ref">.reuse/dep5</span> following [Debian dep5 specification](https://dep-team.pages.debian.net/deps/dep5/) (see examples below);
    - you MUST NOT use wildcards (\*) in dep5 "Files" paragraphs even if Debian specs allow it: it may lead to unnoticed errors or inconsistencies in case of future file additions that may be covered by wildcard expressions even if they have a different license;
    - in case of doubts or problems in finding the correct license and copyright information for Third Party Files, contributors may ask the project's Legal Team in the project mailing list <oniro-dev@eclipse.org>;

### SPDX Header Example

Make sure all of your submitted new files have a licensing statement in the headers. Please make sure that the license for your file is consistent with the licensing choice at project level and that you select the correct SPDX identifier, as in the following example for Apache 2.0 license:

``` text
/*
 * SPDX-FileCopyrightText: Jane Doe <jane@example.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 */
```

### DEP5 "Files" Paragraph Examples

``` text
Files: meta-oniro-staging/recipes-containers/buildah/buildah_git.bb
Copyright: OpenEmbedded Contributors
License: MIT
Comment: Recipe file for buildah copied from meta-virtualization project at
         https://git.yoctoproject.org/meta-virtualization,
         recipes-containers/buildah.
         README file of meta-virtualization project states:
         "All metadata is MIT licensed unless otherwise stated."

Files: meta-oniro-staging/recipes-devtools/ninja/ninja/0001-feat-support-cpu-limit-by-cgroups-on-linux.patch
Copyright: Google Inc.
License: Apache-2.0
Comment: Patch for ninja backported from Ninja project at
        https://github.com/ninja-build/ninja, commit 540be33
        Copyright text left as found in the header of the patched file.
```

### Substantial Contributions

Therefore, if your contribution is only a patch directly applied to an existing file, then you are not required to do anything. If your contribution is an entire new project, or a substantial, copyrighted contribution, you **MUST** make sure that you do that following the [IP Policy](https://git.ostc-eu.org/oss-compliance/ip-policy/) and that you comply with REUSE standard to include the licensing information where they are required.

# DCO sign-off

## Overview

Commits **MUST** be submitted only with a sign-off by the submitter. A commit without a sign-off will be automatically rejected. You don't need be the author or the copyright holder of the contribution, but you must make sure that you have the power to submit on behalf of those who are.

To sign your work, just add a line like this at the end of your commit message:

``` text
Signed-off-by: Jane Doe <jane@example.com>
```

This could be done automatically in the `git` submission:

``` text
git commit --signoff -m "comment"
```

## Developer Certificate of Origin

By doing this you state that you certify the following (from [https://developercertificate.org](https://developercertificate.org/)):

``` text
Developer Certificate of Origin
Version 1.1

Copyright (C) 2004, 2006 The Linux Foundation and its contributors.
1 Letterman Drive
Suite D4700
San Francisco, CA, 94129

Everyone is permitted to copy and distribute verbatim copies of this
license document, but changing it is not allowed.


Developer's Certificate of Origin 1.1

By making a contribution to this project, I certify that:

(a) The contribution was created in whole or in part by me and I
    have the right to submit it under the open source license
    indicated in the file; or

(b) The contribution is based upon previous work that, to the best
    of my knowledge, is covered under an appropriate open source
    license and I have the right under that license to submit that
    work with modifications, whether created in whole or in part
    by me, under the same open source license (unless I am
    permitted to submit under a different license), as indicated
    in the file; or

(c) The contribution was provided directly to me by some other
    person who certified (a), (b) or (c) and I have not modified
    it.

(d) I understand and agree that this project and the contribution
    are public and that a record of the contribution (including all
    personal information I submit with it, including my sign-off) is
    maintained indefinitely and may be redistributed consistent with
    this project or the open source license(s) involved.
```

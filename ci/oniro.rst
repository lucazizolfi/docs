.. SPDX-FileCopyrightText: Huawei Inc.
..
.. SPDX-License-Identifier: CC-BY-4.0

.. include:: ../definitions.rst

The oniro Repository
====================

.. _oniro repository: https://gitlab.eclipse.org/eclipse/oniro-core/oniro.git

The `oniro repository`_ contains meta-layers specific to |main_project_name| and is
the most important repository.

The CI pipeline is defined in the file ``.gitlab-ci.yml``.

Shared Job Definitions
----------------------

The *oniro* repository maintains the list of configurations to
build. That list includes all the builds jobs, covering all the supported
configurations.

The pipeline customizes the ``.build`` job to allow the build process to take
into account any changes being introduced to the `oniro` repository by the
incoming pull request. This is done by setting the ``CI_ONIRO_GIT_REPO_PATH``
variable, which is used by the ``.workspace`` job defined in the *oniro*
repository.

Special Jobs
------------

build-docs
..........

This job runs whenever a merge request is made, or when changes land in the
default branch and in addition, the ``docs/`` directory is modified. This job
builds the documentation with sphinx and ensures it can be built without any
warnings or errors.

update-docs
...........

This job runs whenever changes land on the default branch and affect either the
pipeline or the ``docs/`` directory. This job *triggers* the pipeline of the
``distro/docs`` repository, ensuring that any change to documentation
present in *oniro* is reflected in the aggregated documentation build
maintained in the *docs* repository.

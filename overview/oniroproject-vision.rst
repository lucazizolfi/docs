.. SPDX-FileCopyrightText: Huawei Inc.
..
.. SPDX-License-Identifier: CC-BY-4.0

.. include:: ../definitions.rst

|main_project_name| Vision and Aims
===================================

System Positioning
------------------

|main_project_name| is an ambitious rethink on how an open source collaborative
operating system can be run across a variety of device classes - from small
microcontrollers with kilobytes of memory to powerful CPUs driving a phone,
laptop or even a data center. The goal of |main_project_name| is to evolve a
set of builtin system capabilities on top of commodity open source kernels that
allows sharing of resources and collaboration across distributed devices of
various classes.

The development of these capabilities will happen through an open community
project along with other interested parties.

- For an end-user, |main_project_name| will integrate the multiple, standalone
  smart devices owned by the user and allow for fast interconnection,
  capability collaboration, and resource sharing between them. This way, the
  individual devices can collaborate to provide better context-aware services
  than if they were operating independently of each other.

- For an application developer, |main_project_name| will integrate distributed
  technologies to ease application development across different device classes.
  Developers will be able to focus on upper-layer service logic and develop
  richer, collaborative applications more easily.

- For device developers, |main_project_name| will provide reference software
  blueprints for the key product verticals that will allow them to focus their
  time on tailoring the OS to their device's resource capabilities and service
  characteristics. These software blueprints will provide best-in-class
  practices and solutions to keep the devices secure out-of-the-box.

|main_project_name| will support APIs in multiple programming languages
depending on the constraints of the underlying hardware. Developers will be
able to choose from Java, Extensible Markup Language (XML), C/C++, JavaScript
(JS), Cascading Style Sheets (CSS), and HarmonyOS Markup Language (HML) to
develop applications for |main_project_name|.

OpenHarmony Compatibility
-------------------------

|main_project_name| will be OpenHarmony compatible. It will include the services
and frameworks required by OpenHarmony specification, and provide the APIs
required by OpenHarmony specification, so that you can develop products that can
interoperate with other OpenHarmony products and be certified as OpenHarmony
compatible.

OpenHarmony compatibility will enable required OpenHarmony features in the
kernel layer, system services layer and framework layer, allowing the addition
of OpenHarmony compatible applications.

Due to the inherent modularity of the Oniro build system (OpenEmbedded),
individual projects will be able to pick and choose from the OpenHarmony
compatibility features, allowing to build products with just the parts that are
needed.

Technical Architecture
----------------------

|main_project_name| has a layered architecture built around the Yocto Project
and bitbake build system. The Yocto Project is very popular in the embedded
Linux community and provides an excellent platform for developing a
highly-customizable, cross-kernel operating system. From bottom to top,
|main_project_name| consists of the kernel layer, system services layer,
framework layer, and application layer. In multi-device development, Yocto
provides the capabilities to tweak layers and recipes to remove unnecessary
subsystems, functions, or modules as required.

.. tabs::

   .. tab:: Kernel Layer

      |main_project_name| will support a multi-kernel design out of the box
      (Linux kernel and an RTOS such as Zephyr RTOS or LiteOS) so that
      appropriate OS kernels can be selected for devices with different
      resource limitations. Over time, a kernel abstraction layer (KAL) will
      shield differences in kernel implementations and provide the upper layer
      with basic kernel capabilities, including process and thread management,
      memory management, file system, network management, and peripheral
      management.

   .. tab:: System Services Layer

      The System Services Layer will contain the bulk of the differentiating
      features of |main_project_name|. It will provide a complete set of
      capabilities essential for |main_project_name| to offer services for
      applications through the framework layer. The system services layer will
      add the following features over time:

      - The protocols and primitives that allow devices to discover each other

      - APIs to allow sharing of computing, storage and other resources

      - APIs that allow applications to be more context-aware due to 
        collaboration with other devices in the network

      - APIs to allow applications to expose business logic as `abilities`
        that may be integrated into other applications or even used on other
        devices in the network

   .. tab:: Framework Layer

      The Framework layer will provide an SDK to develop |main_project_name|
      applications in multiple languages such as Java, C, C++, and JS depending
      on the target device class and its HW constraints.

   .. tab:: Application Layer

      When completed, the Application layer will host the system and
      third-party applications. |main_project_name| applications will be able
      to use APIs to expose business logic as `abilities` that may be utilized
      inside other applications, thus allowing creation of more integrated 
      experiences on the same device as well as distributed across devices.
